//
//  value.cpp
//  TCPPushServer
//
//  Created by 张琪 on 15/2/10.
//  Copyright (c) 2015年 telecom. All rights reserved.
//

#include "value.h"

#include <regex>
using namespace util;

string util::null_str;

const value value::empty=false;

bool util::isnumber(const string & str){
    regex pattern("(^[-|+]?\\d+(\\.\\d+)?$)");
    smatch match;
    
    return regex_match(str, match, pattern);
}

bool util::ishex(const string & str){
    regex pattern("(^0x([0-9]|[a-f]|[A-F])*$)");
    smatch match;
    
    return regex_match(str, match, pattern);
}

bool util::isfloat(const string & str){
    regex pattern("^[-|+]?\\d+\\.\\d+$");
    smatch match;
    
    return regex_match(str, match, pattern);
}

bool util::isinteger(const string & str){
    regex pattern("^[-|+]?\\d+$");
    smatch match;
    
    return regex_match(str, match, pattern);
}

string util::tolower(const string & str){
    string ret;
    ret.resize(str.length());
    transform(str.begin(), str.end(), ret.begin(), ::tolower);
    return ret;
}

value::value(){
    _type = value_type_null;
}

value::value(long val){
    _type = value_type_int;
    _long = val;
}

value::value(double val){
    _type = value_type_float;
    _double = val;
}

value::value(unsigned long val){
    _type = value_type_uint;
    _ulong = val;
}

value::value(bool val){
    _type = value_type_bool;
    _bool = val;
}

value::value(const char * val):value(string(val)){}

value::value(string val){
    val = trim(val);
    if (ishex(val)) {
        _type = value_type_uint;
        stringstream ss;
        ss<<hex<<val;
        ss>>_ulong;
    }else if(isfloat(val)){
        _type = value_type_float;
        _double = stod(val);
    }else if(isinteger(val)){
        _type = value_type_int;
        _long = stol(val);
    }else if(val=="true" || val=="false" || val=="yes" || val=="no"){
        _type = value_type_bool;
        _bool = val=="true" || val=="yes";
    }else{
        _type = value_type_string;
        _string = val;
    }
}

value::value(const void * bytes, size_t sz, const char * name){
    _type = value_type_package;
    _pkg.bytes = malloc(sz);
    memcpy(_pkg.bytes, bytes, sz);
    _pkg.size = sz;
    
    if (name!=0) {
        _string = name;
    }else{
        stringstream ss;
        ss<<"package "<<sz<<" bytes";
        _string = ss.str();
    }
}

bool value::operator==(const util::value &other) const{
    if (_type!=other._type) return false;
    
    switch (_type) {
        case value_type_bool:
            return _bool == other._bool;
            
        case value_type_int:
            return _long == other._long;
            
        case value_type_uint:
            return _ulong == other._ulong;
            
        case value_type_float:
            return _double == other._double;
            
        case value_type_string:
            return _string == other._string;
            
        case value_type_null:
            return true;
            
        case value_type_package:{
            if (_pkg.size!=other._pkg.size) return false;
            unsigned char * bytes1 = (unsigned char *)_pkg.bytes;
            unsigned char * bytes2 = (unsigned char *)other._pkg.bytes;
            
            for (size_t idx = 0; idx<_pkg.size; idx++) {
                if (bytes1[idx]!=bytes2[idx]) return false;
            }
            
            return true;
        }break;
            
        default:
            return false;
    }
}

void value::operator=(const util::value &other){
    _type = other._type;
    
    _string.clear();
    
    switch (_type) {
        case value_type_bool:
            _bool = other._bool;
            break;
            
        case value_type_int:
            _long = other._long;
            break;
            
        case value_type_uint:
            _ulong = other._ulong;
            break;
            
        case value_type_float:
            _double = other._double;
            break;
            
        case value_type_string:
            _string = other._string;
            break;
            
        case value_type_package:
            _pkg.size = other._pkg.size;
            _pkg.bytes = realloc(_pkg.bytes, _pkg.size);
            memcpy(_pkg.bytes, other._pkg.bytes, _pkg.size);
            break;
            
        default:break;
    }
}

value_type value::type() const{
    return _type;
}

long value::longVal() const{
    return _type==value_type_int ? _long : 0;
}

unsigned long value::ulongVal() const{
    return _type==value_type_uint ? _ulong : 0;
}

double value::doubleVal() const{
    return _type==value_type_float ? _double : 0;
}

bool value::boolVal() const{
    return _type==value_type_bool ? _bool : false;
}

const string & value::stringVal() const{
    static string emptystr;
    return _type==value_type_string ? _string : emptystr;
}

const void * value::packageBytes() const{
    return _type==value_type_package ? nullptr : _pkg.bytes;
}

size_t value::packageSize() const{
    return _type==value_type_package ? 0 : _pkg.size;
}

long value::asLong() const{
    
    switch (_type) {
        case value_type_int:
            return _long;
            
        case value_type_float:
            return _double;
            
        case value_type_uint:
            return _ulong;
            
        case value_type_bool:
            return _bool ? 1 : 0;
            
        case value_type_string:
        {
        if (isnumber(_string)) {
            return stol(_string);
        }else if(ishex(_string)) {
            stringstream ss;
            long val;
            ss<<hex<<_string;
            ss>>val;
            return val;
        }else{
            return 0;
        }
        }break;
            
        case value_type_package:
            return _pkg.size;
            
        default: return 0;
    }
}

unsigned long value::asULong() const{
    
    switch (_type) {
        case value_type_int:
            return _long;
            
        case value_type_float:
            return _double;
            
        case value_type_uint:
            return _ulong;
            
        case value_type_bool:
            return _bool ? 1 : 0;
            
        case value_type_string:
        {
        if (isnumber(_string)) {
            return stol(_string);
        }else if(ishex(_string)) {
            stringstream ss;
            long val;
            ss<<hex<<_string;
            ss>>val;
            return val;
        }else{
            return 0;
        }
        }break;
            
        case value_type_package:
            return _pkg.size;
            
        default:
            return 0;
            break;
    }
}

double value::asDouble() const{
    switch (_type) {
        case value_type_int:
            return _long;
            
        case value_type_uint:
            return _ulong;
            
        case value_type_float:
            return _double;
            
        case value_type_bool:
            return _bool ? 1 : 0;
            
        case value_type_string:
        {
        if (isnumber(_string)) {
            return stod(_string);
        }else{
            return 0;
        }
        }break;
            
        case value_type_package:
            return _pkg.size;
            
        default:
            return 0;
    }
}

bool value::asBool() const{
    switch (_type) {
        case value_type_int:
            return _long>0;
            
        case value_type_uint:
            return _ulong>0;
            
        case value_type_float:
            return _double>0;
            
        case value_type_bool:
            return _bool;
            
        case value_type_string:
        {
        string lower = tolower(_string);
        return lower=="true" || lower=="yes";
        }break;
            
        case value_type_package:
            return _pkg.size>0;
            
        default: return false;
    }
}

string value::asString() const{
    switch (_type) {
        case value_type_int:
            return to_string(_long);
            
        case value_type_uint:
            return to_string(_ulong);
            
        case value_type_float:
            return to_string(_double);
            
        case value_type_bool:
            return _bool ? "true" : "false";
            
        case value_type_string:
            return _string;
            
        case value_type_package:
            return _string;
            
        default: return null_str;
    }
}