//
//  Server.h
//  TCPPushServer
//
//  Created by 张琪 on 15/2/5.
//  Copyright (c) 2015年 telecom. All rights reserved.
//

#ifndef __TCPPushServer__Server__
#define __TCPPushServer__Server__

#include "common.h"
#include "Client.h"

class Server{
    event_base * _evb = nullptr;
    evconnlistener * _listener = nullptr;
    
    std::list<Client*> _clients;
    
    size_t _conn_limit = 50000;
    
    static void connCallback(struct evconnlistener *,
                             evutil_socket_t,
                             struct sockaddr *,
                             int socklen, void *);

    static void errCallback(struct evconnlistener *, void *);
    
    static void timerCallback(int sock, short which, void *arg);
    
    void onConnect(evutil_socket_t,const sockaddr_in &);
    void onDisconnect(Client *);
    void onReceive(Client *, const void *, size_t);
    void onError();
    
protected:
    virtual void onClientConnected(Client *) {}
    virtual void onClientDisconnected(Client *) {}
    virtual void onClientReceive(Client *, const void *, size_t) {}
    
public:
    Server();
    ~Server();
    
    void delay(std::function<void()>,const timeval &);
    
    bool listen(int port);
    void stop();
};

#endif /* defined(__TCPPushServer__Server__) */
