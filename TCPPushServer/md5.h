//
//  md5.h
//  TCPPushServer
//
//  Created by 张琪 on 15/2/11.
//  Copyright (c) 2015年 telecom. All rights reserved.
//

#ifndef __TCPPushServer__md5__
#define __TCPPushServer__md5__

#include "common.h"

std::string md5(const std::string &);

#endif /* defined(__TCPPushServer__md5__) */
