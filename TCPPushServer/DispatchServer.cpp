//
//  DispatchServer.cpp
//  TCPPushServer
//
//  Created by 张琪 on 15/2/6.
//  Copyright (c) 2015年 telecom. All rights reserved.
//

#include "DispatchServer.h"
#include <sys/stat.h>
#include "md5.h"

using namespace std;
using namespace std::placeholders;

#define TagKeyToken "token"

#define ServerKey "kjsdhrfg*&^guy34ernueu&^%$765egji"
#define ClientKey "peo4irth&^8674hblh(*67wegtdo*&^87bhj"

string validate(const string & content, const char * key){
    cout<<"validating input "<<content<<endl;
    
    timeval _t;
    gettimeofday(&_t, 0);
    auto time = _t.tv_sec>>4;

    cout<<"timestamp is "<<time<<endl;

    stringstream ss;
    ss<<time<<content<<key;

    cout<<"input composed as "<<ss.str()<<endl;

    string sig = ::md5(ss.str());

    cout<<"validated signature is "<<sig<<endl;

    return sig;
}

void DispatchServer::removeToken(Client *client){
    const auto & token = client->getTag(TagKeyToken);
    if (!token.empty()) {
        _clients[token].remove(client);
    }
}

void DispatchServer::onClientDisconnected(Client * client){
    cout<<"remote "<<client->getRemoteAddress()
    <<" disconnected"<<endl;
    removeToken(client);
}

void DispatchServer::onClientConnected(Client * client){
    cout<<"remote "<<client->getRemoteAddress()<<" connected"<<endl;
    delay([client](){
        if (!Client::isDeleted(client)) {
            const auto & token = client->getTag(TagKeyToken);
            if (token.empty()) {
                cout<<"remote "<<client->getRemoteAddress()<<" timeout"<<endl;
                client->close();
            }
        }
    }, timeval{.tv_sec=10});
}

#define imp_service(name) void DispatchServer::service_##name(Client * client, const std::string & query)

imp_service(bye){
    removeToken(client);
    
    client->send("succeed");
    client->close();
}

imp_service(checkin){
    // client check in
    cout<<"client checkin..."<<endl;
    
    stringstream ss(query);
    string signature,token;
    ss>>signature>>token;

    cout<<"signature is "<<signature<<endl;

    if (validate("checkin "+token, ClientKey)==signature) {
	cout<<"signature is valied"<<endl;

        client->setTag(TagKeyToken, token);
        _clients[token].push_back(client);
        cout<<"remote "<<client->getRemoteAddress()
        <<" checking as #"<<token
        <<" succeed"<<endl;
    }else{
        cout<<"remote "<<client->getRemoteAddress()
        <<" checking as #"<<token
        <<" failed"<<endl;
        client->close();
    }
}

imp_service(send){
    string raw = query;
    string signature,token,message;
    
    if (read_input(raw, signature, raw) &&
        read_input(raw, token, message)) {
        stringstream ss;
        ss<<"send "<<token<<' '<<message;
        if (validate(ss.str(), ServerKey)==signature) {
            for (auto client : _clients[token]) {
                client->send(message);
            }
            cout<<"remote "<<client->getRemoteAddress()
            <<" send msg "<<message
            <<" to #"<<token<<endl;
        }else{
            cout<<"remote "<<client->getRemoteAddress()
            <<" sending validate failed"<<endl;
        }
    }
}

#define pub_service(name) publicService(#name,bind(&DispatchServer::service_##name,this,_1,_2))

DispatchServer::DispatchServer(){
    
    pub_service(bye);
    pub_service(send);
    pub_service(checkin);
}
