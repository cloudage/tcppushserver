//
//  Client.cpp
//  TCPPushServer
//
//  Created by 张琪 on 15/2/5.
//  Copyright (c) 2015年 telecom. All rights reserved.
//

#include "Client.h"

using namespace std;

set<const Client*> _inst;

Client::Client(event_base * base, evutil_socket_t socket, const sockaddr_in & address){
    _socket = socket;
    _address = address;
    _event = bufferevent_socket_new(base, socket, BEV_OPT_CLOSE_ON_FREE);
    
    std::stringstream ss;
    ss<<inet_ntoa(address.sin_addr)<<':'<<address.sin_port;
    _address_str = ss.str();
    
    bufferevent_setcb(_event, &readCallback, nullptr, &eventCallback, this);
    
    bufferevent_enable(_event, EV_READ|EV_WRITE);
    
    _inst.insert(this);
}

Client::~Client(){
    _inst.erase(this);
    if (_event!=nullptr) {
        bufferevent_free(_event);
    }
}

bool Client::isDeleted(const Client * client){
    return _inst.find(client)==_inst.end();
}

void Client::readCallback(bufferevent *, void * ctx){
    ((Client*)ctx)->onReceive();
}

void Client::eventCallback(bufferevent *, short e, void * ctx){
    Client * client = (Client*)ctx;
    
    if (e & BEV_EVENT_ERROR) {
        client->onError();
    }
    
    if (e & (BEV_EVENT_EOF|BEV_EVENT_ERROR)) {
        client->onRemoteClose();
    }
}

bool Client::send(const void *data, size_t len){
    return bufferevent_write(_event, data, len)==0;
}

void Client::onReceive(){
    evbuffer * input = bufferevent_get_input(_event);
    
    evbuffer_peek(input, -1, nullptr, &_iovec, 1);
    
    callbacks.receive(this,_iovec.iov_base,_iovec.iov_len);

    evbuffer_drain(input, -1);
    
//    evbuffer * output = bufferevent_get_output(_event);
//    evbuffer_add_printf(output, "echo:");
//    evbuffer_add(output, _buff, len);
}

void Client::onError(){
    callbacks.error(this);
}

void Client::onRemoteClose(){
    callbacks.closed(this);
}

void Client::close(){
    bufferevent_free(_event);
    _event = nullptr;
}