//
//  MessageServer.h
//  TCPPushServer
//
//  Created by 张琪 on 15/2/6.
//  Copyright (c) 2015年 telecom. All rights reserved.
//

#ifndef __TCPPushServer__MessageServer__
#define __TCPPushServer__MessageServer__

#include "Server.h"

class MessageServer : public Server{
public:
    typedef std::function<void(Client*, const std::string &)> func_service;
    
private:
    std::map<std::string, func_service> _services;
    void call(Client *, const std::string &, const std::string &);
    
protected:
    void publicService(const std::string & name, func_service srv);
    
public:
    virtual void onClientReceive(Client *, const void *, size_t) override;
};

#endif /* defined(__TCPPushServer__MessageServer__) */
