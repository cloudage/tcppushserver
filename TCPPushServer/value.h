//
//  value.h
//  TCPPushServer
//
//  Created by 张琪 on 15/2/10.
//  Copyright (c) 2015年 telecom. All rights reserved.
//

#ifndef __TCPPushServer__value__
#define __TCPPushServer__value__

#include "common.h"

namespace util {
    
    using namespace std;
    
    enum value_type{
        value_type_null = 0,
        value_type_int,
        value_type_uint,
        value_type_float,
        value_type_bool,
        value_type_string,
        value_type_package,
    };
    
    class value{
        union{
            long _long;
            unsigned long _ulong;
            double _double;
            bool _bool;
            
            struct{
                void * bytes;
                size_t size;
            }_pkg;
        };
        
        string _string;
        
        value_type _type;
        
    public:
        value();
        value(long val);
        value(unsigned long val);
        value(double val);
        value(bool val);
        value(string val);
        value(const char * val);
        value(const void *,size_t,const char * =0);
        
        value_type type() const;
        
        long longVal() const;
        unsigned long ulongVal() const;
        double doubleVal() const;
        bool boolVal() const;
        const string & stringVal() const;
        
        const void * packageBytes() const;
        size_t packageSize() const;
        
        long asLong() const;
        unsigned long asULong() const;
        double asDouble() const;
        bool asBool() const;
        string asString() const;
        
        bool operator==(const value & other) const;
        void operator=(const value & other);
        
        inline value(int val):value((long)val) {}
        inline int intVal() const { return (int)longVal(); }
        inline int asInt() const { return (int)asLong(); }
        
        inline value(unsigned int val):value((unsigned long)val) {}
        inline unsigned int uintVal() const { return (unsigned int)ulongVal(); }
        inline unsigned int asUInt() const { return (unsigned int)asULong(); }
        
        inline value(float val):value((double)val) {}
        inline float floatVal() const { return (float)doubleVal(); }
        inline float asFloat() const { return (float)asDouble(); }
        
        inline value(char val):value((long)val) {}
        inline char charVal() const { return (char)longVal(); }
        inline char asChar() const { return (char)asLong(); }
        
        template<typename T> value(typename enable_if<is_class<T>::value,T>::type v){
            
        }
        
        static const value empty;
    };
    
    extern string null_str;
    bool isnumber(const string &);
    bool ishex(const string &);
    bool isfloat(const string &);
    bool isinteger(const string &);
    
    string tolower(const string &);
}
#endif /* defined(__TCPPushServer__value__) */
