//
//  main.cpp
//  TCPPushServer
//
//  Created by 张琪 on 15/2/5.
//  Copyright (c) 2015年 telecom. All rights reserved.
//

#include "DispatchServer.h"

using namespace std;

int main(int argc, const char * argv[]) {
    
    DispatchServer().listen(7777);
    
    return 0;
}
