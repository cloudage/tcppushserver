//
//  Client.h
//  TCPPushServer
//
//  Created by 张琪 on 15/2/5.
//  Copyright (c) 2015年 telecom. All rights reserved.
//

#ifndef __TCPPushServer__Client__
#define __TCPPushServer__Client__

#include "common.h"
#include "value.h"

class Client{
    evutil_socket_t _socket;
    bufferevent * _event;
    sockaddr_in _address;
    evbuffer_iovec _iovec;
    std::string _address_str;

    std::map<std::string,std::string> _tag;
    
    static void readCallback(bufferevent*,void*);
    static void eventCallback(bufferevent*,short,void*);
    
    void onError();
    void onRemoteClose();
    void onReceive();

public:

    struct{
        std::function<void(Client*)> error = std::bind([](){});
        std::function<void(Client*)> closed = std::bind([](){});
        std::function<void(Client*,const void *,size_t)> receive = std::bind([](){});
    }callbacks;
    
    /**
     Make sure that the socket you provide is in non-blocking mode.
     use evutil_make_socket_nonblocking(int).
     */
    
    Client(event_base *, evutil_socket_t, const sockaddr_in &);
    ~Client();

    void close();
    
    bool send(const void * data, size_t len);

    inline const std::string & getTag(const std::string & key) const;
    inline void setTag(const std::string & key, const std::string &);
    
    inline bool send(const std::string & msg);
    
    inline bool send(const char *);
    
    inline const std::string & getRemoteAddress() const;

    static bool isDeleted(const Client *);
};

inline bool Client::send(const std::string & msg){
    return send(msg.c_str(), msg.length());
}

inline bool Client::send(const char * str){
    return send((const void *)str, strlen(str));
}

inline const std::string & Client::getRemoteAddress() const{
    return _address_str;
}

inline const std::string & Client::getTag(const std::string & key) const{
    auto iter = _tag.find(key);
    static std::string empty;
    return iter==_tag.end() ? empty : iter->second;
}

inline void Client::setTag(const std::string & key, const std::string & tag){
    if(tag.empty()){
        _tag.erase(key);
    }else{
        _tag[key] = tag;
    }
}

#endif /* defined(__TCPPushServer__Client__) */
