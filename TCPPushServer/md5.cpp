//
//  md5.cpp
//  TCPPushServer
//
//  Created by 张琪 on 15/2/11.
//  Copyright (c) 2015年 telecom. All rights reserved.
//

#include "md5.h"

#include <openssl/md5.h>
#include <unistd.h>
#include <iomanip>

using namespace std;

string md5(const string & src){
    int n;
    MD5_CTX c;

    MD5_Init(&c);

    MD5_Update(&c, src.c_str(), src.length());

    unsigned char result[MD5_DIGEST_LENGTH];
    
    MD5_Final(result, &c);
    
    stringstream ss;
    for(n=0; n<MD5_DIGEST_LENGTH; n++){
        ss<<hex<<setw(2)<<setfill('0')<<(unsigned int)result[n];
    }

    return ss.str();
}