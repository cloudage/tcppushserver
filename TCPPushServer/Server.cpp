//
//  Server.cpp
//  TCPPushServer
//
//  Created by 张琪 on 15/2/5.
//  Copyright (c) 2015年 telecom. All rights reserved.
//

#include "Server.h"
#include "Client.h"

Server::Server(){
    std::cout<<"using libevent "<<event_get_version()<<std::endl;
    
    _evb = event_base_new();
    
    event_base_priority_init(_evb, EVENT_MAX_PRIORITIES-1);
    
    std::cout<<"using method "<<event_base_get_method(_evb)<<std::endl;
}

Server::~Server(){
    if (_listener!=nullptr) {
        evconnlistener_free(_listener);
    }
    
    for (Client * client : _clients) {
        delete client;
    }
    
    _clients.clear();
    
    event_base_free(_evb);
}

bool Server::listen(int port){
    evutil_socket_t sock = socket(AF_INET, SOCK_STREAM, 0);

    if(evutil_make_socket_nonblocking(sock)==-1){
        std::cerr<<"failed to make server socket non blocking"<<std::endl;
        return false;
    };
    
    sockaddr_in serverAddress;
    memset(&serverAddress, 0, sizeof(sockaddr_in));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port = htons(port);

    _listener = evconnlistener_new_bind(_evb,
                                        &connCallback,
                                        this,
                                        LEV_OPT_CLOSE_ON_FREE|LEV_OPT_REUSEABLE,
                                        -1,
                                        (sockaddr*)&serverAddress,
                                        sizeof(sockaddr_in));
    
    
    if (_listener==nullptr) {
        std::cerr<<"failed bind to port "<<port<<std::endl;
        return false;
    }
    
    int result = event_base_dispatch(_evb);
    
    if (result==-1) {
        std::cerr<<"event loop exited because of some unhandled error in the backend"<<std::endl;
        return false;
    }else if(result==1){
        std::cout<<"event loop exited because there were no more pending or active events"<<std::endl;
        return false;
    }else{
        return true;
    }
}

void Server::stop(){
    event_base_loopbreak(_evb);
}

void Server::connCallback(struct evconnlistener *,
                          evutil_socket_t sock,
                          struct sockaddr * addr,
                          int socklen,
                          void * ptr){
    ( (Server*)ptr )->onConnect(sock, *(sockaddr_in*)addr);
}

void Server::errCallback(struct evconnlistener *, void * ptr){
    ((Server*)ptr)->onError();
}

void Server::onConnect(evutil_socket_t socket,const sockaddr_in & address){


    std::cout<<"incoming new client"<<std::endl;
    
    if (_clients.size()>=_conn_limit) {
        std::cout<<"but connection count reached the limit."<<std::endl;
    }else{
        Client * client = new Client(_evb, socket, address);
        
        std::cout<<"accpeted from "<<client->getRemoteAddress()<<std::endl;
        
        _clients.push_back(client);
        
        client->callbacks.error = std::bind(&Server::onDisconnect, this, ::std::placeholders::_1);
        client->callbacks.closed = std::bind(&Server::onDisconnect, this, ::std::placeholders::_1);
        client->callbacks.receive = std::bind(&Server::onReceive, this,
                                              ::std::placeholders::_1,
                                              ::std::placeholders::_2,
                                              ::std::placeholders::_3);
        
        onClientConnected(client);
    }
}

struct ServerDelayParams{
    Server * server;
    event * ev;
    std::function<void()> callback;
};

void Server::timerCallback(int sock, short which, void *arg){
    auto param = (ServerDelayParams*)arg;
    
    param->callback();
    
    evtimer_del(param->ev);
    delete param;
}

void Server::delay(std::function<void ()> func, const timeval & t){
    auto param = new ServerDelayParams{
        .server = this,
        .callback = func,
    };

    auto tv = evtimer_new(_evb, Server::timerCallback, param);
    
    param->ev = tv;
    
    evtimer_add(tv, &t);
}

void Server::onDisconnect(Client * client){
#ifdef DEBUG
    std::cout<<"client @"<<client->getRemoteAddress()<<" disconnected"<<std::endl;
#endif
    
    for (auto i = _clients.begin(); i!=_clients.end(); i++) {
        if (*i==client) {
            _clients.erase(i);
            break;
        }
    }
    
    onClientDisconnected(client);
    
    delete client;
}

void Server::onError(){
#ifdef DEBUG
    int err = EVUTIL_SOCKET_ERROR();
    std::cerr<<"server got an error "<<evutil_socket_error_to_string(err)<<std::endl;
#endif
    stop();
}

void Server::onReceive(Client * client, const void * data, size_t size){
#ifdef DEBUG
    std::cout<<"receive "<<size<<" bytes from "<<" @"<<client->getRemoteAddress()<<std::endl;
#endif
    onClientReceive(client, data, size);
}