//
//  common.h
//  TCPPushServer
//
//  Created by 张琪 on 15/2/5.
//  Copyright (c) 2015年 telecom. All rights reserved.
//

#ifndef TCPPushServer_common_h
#define TCPPushServer_common_h

#include <string.h>

#include <iostream>
#include <event2/event.h>
#include <event2/util.h>
#include <event2/buffer.h>
#include <event2/bufferevent.h>
#include <event2/listener.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <arpa/inet.h>

#include <uuid/uuid.h>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <functional>

class Server;
class Client;

namespace std {
    
    /**
     the cond should returns zero if the character tests false and returns non-zero if the character
     tests true
     */
    inline string trim(const string & v,function<int(int)> cond=::isspace){
        string str;
        
        bool trimming_head = true;
        size_t cur = 0;
        size_t cur_s = 0;
        size_t srclen = v.length();
        
        for (size_t i=0; i<srclen; i++) {
            auto ch = v[i];
            
            if (trimming_head) {
                if (cond(ch)==false){
                    trimming_head = false;
                    str.push_back(ch);
                    cur_s = i;
                }else continue;
            }else{
                if (cond(ch)==false) {
                    cur = i;
                }
                
                str.push_back(ch);
            }
        }
        
        return move(str.substr(0,cur-cur_s+1));
    }
    
    inline string trim(const string & v, char c){
        return trim(v,[c](int ch)->int{ return ch==c; });
    }
    
    inline string trim(const string & str, const string & chars){
        return trim(str,[chars](int ch)->int{
            return chars.find(ch)!=chars.npos;
        });
    }
    
    inline bool has_prefix(const string &str, const string &head){
        auto istr = str.begin();
        auto ihead = head.begin();
        
        while (ihead!=head.end()) {
            if (istr==str.end() || *istr!=*ihead) {
                return false;
            }
            
            ihead++;
            istr++;
        }
        
        return true;
    }
    
    inline vector<string> explode(const string & v,function<int(int)> exp=::isspace){
        
        vector<string> ret;
        string found;
        size_t srclen = v.size();
        
        for(size_t i = 0;i<srclen;i++){
            char ch = v.at(i);
            if (exp(ch)!=0 && found.length()>0) {
                ret.push_back(found);
                found.clear();
            }else if(exp(ch)==0){
                found.push_back(ch);
            }
        }
        
        if (found.length()>0) {
            ret.push_back(found);
        }
        
        return move(ret);
    }
    
    inline map<string,string> make_params(const string & str){
        vector<string> pairs = explode(str, [](int ch)->int{ return ch=='&'; });
        map<string,string> params;
        
        for (auto pair : pairs) {
            auto kv = explode(pair, [](int ch)->int{ return ch=='='; });
            if (kv.size()==2) {
                params[trim(kv[0])] = trim(kv[1]);
            }
        }
        
        return move(params);
    }
    
    inline bool read_line(string line, pair<string,string> & kv){
        line = trim(line);
        auto firstEql = line.find('=');
        if (firstEql==string::npos) return false;
        
        kv.first = trim(line.substr(0,firstEql));
        kv.second = trim(line.substr(firstEql+1,line.size()-firstEql));
        
        return true;
    }
    
    inline map<string,string> read_config(istream & stm){
        string line;
        map<string,string> kvm;
        
        while (getline(stm, line)) {
            pair<string,string> kv;

            if (read_line(line, kv)) kvm.insert(kv);
        }
        
        return move(kvm);
    }
    
    inline bool read_input(string line,string & output, string & remains){
        auto firstSpace = line.find(' ');
        if(firstSpace==string::npos){
            if (line.empty()) {
                return false;
            }else{
                output = line;
                return true;
            }
        }
        else{
            output = trim(line.substr(0,firstSpace));
            remains = trim(line.substr(firstSpace));
            return true;
        }
    }
}

#endif
