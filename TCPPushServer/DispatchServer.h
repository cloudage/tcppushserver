//
//  DispatchServer.h
//  TCPPushServer
//
//  Created by 张琪 on 15/2/6.
//  Copyright (c) 2015年 telecom. All rights reserved.
//

#ifndef __TCPPushServer__DispatchServer__
#define __TCPPushServer__DispatchServer__

#include "MessageServer.h"

class DispatchServer : public MessageServer{
    
    std::map<std::string,std::list<Client *>> _clients;
    
    virtual void onClientConnected(Client *) override;
    virtual void onClientDisconnected(Client *) override;
    
    void removeToken(Client * client);
    
public:
    DispatchServer();
    
#define decl_service(name) void service_##name(Client* client, const std::string & params)
    
    /**
     disconnect from client
     */
    decl_service(bye);
    
    /**
     client checkin with token and client key
     checkin {signature(checkin token)} {token}
     */
    decl_service(checkin);
    
    /**
     send message to a token with server key
     send {signature(send token message)} {token} {message}
     */
    decl_service(send);
#undef decl_service
    
};

#endif /* defined(__TCPPushServer__DispatchServer__) */
