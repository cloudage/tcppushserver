//
//  MessageServer.cpp
//  TCPPushServer
//
//  Created by 张琪 on 15/2/6.
//  Copyright (c) 2015年 telecom. All rights reserved.
//

#include "MessageServer.h"

using namespace std;

void MessageServer::publicService(const std::string &name, func_service srv){
    _services[name] = srv;
}

void MessageServer::onClientReceive(Client * client, const void * data, size_t sz){
    string raw((const char*)data,sz);

    cout<<client->getRemoteAddress()<<":"<<raw<<endl;

    string cmd;
    string query;
    if (read_input(raw, cmd, query)) {
        call(client,cmd, query);
    }else{
        call(client,raw,"");
    }
}

void MessageServer::call(Client * client, const std::string & cmd, const std::string & query){
    auto imethod = _services.find(cmd);
    if (imethod!=_services.end()) {
        imethod->second(client,query);
    }else{
        client->send("error 404");
    }
}
